import { Creator } from "./models/factories/creator";
import { ConcreteCreator1 } from "./models/factories/concrete-creator-1";
import { ConcreteCreator2 } from "./models/factories/concrete-creator-2";

const results = [];

/**
 * The client code works with an instance of a concrete creator, albeit through
 * its base interface. As long as the client keeps working with the creator via
 * the base interface, you can pass it any creator's subclass.
 */
function clientCode(creator: Creator) {
    console.log('|  Inherits from Creator Abstract Class                                                    |');
    console.log('|  Invoke someOperation method                                                             |');
    console.log('|                                                                                          |');
    console.log(creator.someOperation());
}


console.log('\n\nFACTORY METHOD:');
/**
 * The Application picks a creator's type depending on the configuration or
 * environment.
 */
console.log('\n--------------------------------------------------------------------------------------------');
console.log('|  ConcreteCreator 1                                                                       |');
console.log('--------------------------------------------------------------------------------------------');
clientCode(new ConcreteCreator1());
console.log('--------------------------------------------------------------------------------------------');

console.log('\n--------------------------------------------------------------------------------------------');
console.log('|  ConcreteCreator 2                                                                       |');
console.log('--------------------------------------------------------------------------------------------');
clientCode(new ConcreteCreator2());
console.log('--------------------------------------------------------------------------------------------');