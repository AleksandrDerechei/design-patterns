import { Product } from "./product";

export class ConcreteProduct2 implements Product {

    public operation(): string {
        return 'ConcreteProduct 2';
    }
}