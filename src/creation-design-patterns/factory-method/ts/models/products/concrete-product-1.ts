import { Product } from "./product";

/**
 * Concrete Products provide various implementations of the Product interface.
 */
export class ConcreteProduct1 implements Product {
    public operation(): string {
        return 'ConcreteProduct 1';
    }
}