import { Creator } from "./creator";
import { Product } from "../products/product";
import { ConcreteProduct2 } from '../products/concrete-product-2';

/**
 * Concrete Creators override the factory method in order to change the
 * resulting product's type.
 */
export class ConcreteCreator2 extends Creator {

    public factoryMethod(): Product {
        return new ConcreteProduct2();
    }
}
