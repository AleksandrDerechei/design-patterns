import { Singleton } from './singleton';

/**
 * The client code.
 */
function clientCode() {
    const s1 = Singleton.getInstance();
    const s2 = Singleton.getInstance();


    console.log('\n\nSINGLETON:');

    console.log('\n--------------------------------------------------------------------------------------------');
    if (s1 === s2) {
        console.log('|  Singleton works, both variables contain the same instance.                              |');
        console.log('--------------------------------------------------------------------------------------------');
    } else {
        console.log('|  Singleton failed, variables contain different instances.                                |');
        console.log('--------------------------------------------------------------------------------------------');
    }
}

clientCode();