function getOutput(str: string, len: number = 92): string {
    const emptyLineWithBounders = `|${new Array(len - 1).join(' ')}|` // helpers for demonstration
    return `${str}${emptyLineWithBounders.slice(str.length)}`
}

/*
Offer a significant performance boost; it is most effective in
situations where the cost of initializing a class instance is high, the
rate of instantiation of a class is high, and the number of
instantiations in use at any one time is low.
*/

export class ReusablePool {
    private static reusables: Reusable [] = [];
    private static instance: ReusablePool;
    private static size: ReusablePool;

    private constructor(size: number) {
        for (let i=0 ; i < size; i++) {
            ReusablePool.reusables.push(new Reusable());
            ReusablePool.size = size;
        }
    }

    static resetSize(size: number): void {
        ReusablePool.reusables = [];
        ReusablePool.instance =  new ReusablePool(size);

        console.log('\n--------------------------------------------------------------------------------------------');
        console.log(getOutput(`|  Reset pool of objects:  ${ReusablePool.reusables.length} elements.`));
        console.log('--------------------------------------------------------------------------------------------');
    }

    static getPool(size: number): ReusablePool {
        if (!ReusablePool.instance) {
            ReusablePool.instance =  new ReusablePool(size);
        }

        console.log('\n--------------------------------------------------------------------------------------------');
        console.log(getOutput(`|  Init pool of objects:  ${ReusablePool.reusables.length} elements.`));
        console.log('--------------------------------------------------------------------------------------------');

        return ReusablePool.instance;
    }

    static acquire(): Reusable {
        if(!ReusablePool.reusables.length) {
            console.log('\n--------------------------------------------------------------------------------------------');
            console.log('|  Pool is empty.                                                                          |');
            console.log('--------------------------------------------------------------------------------------------');
            return null; // todo: Could be implemented way to return Promise or observer with released object
        }
        const result = ReusablePool.reusables.pop();

        console.log('\n--------------------------------------------------------------------------------------------');
        console.log(getOutput(`|  Invoke method acquire():  ${ReusablePool.reusables.length} elements.`));
        console.log('--------------------------------------------------------------------------------------------');
        return result
    }

    static release(object: Reusable): void {
        if (ReusablePool.reusables.length === ReusablePool.size) {
            console.log('\n--------------------------------------------------------------------------------------------');
            console.log('|  Pool is full.                                                                           |');
            console.log('--------------------------------------------------------------------------------------------');
            return;
        }
        ReusablePool.reusables.push(object);
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log(getOutput(`|  Invoke method release():  ${ReusablePool.reusables.length} elements.`));
        console.log('--------------------------------------------------------------------------------------------');
    }
}

export class Reusable {}

function clientCode() {
    ReusablePool.getPool(10);
    const reusable10 = ReusablePool.acquire();
    ReusablePool.release(reusable10);

    ReusablePool.resetSize(1);
    const reusable1 = ReusablePool.acquire();
    const reusable2 = ReusablePool.acquire();
    ReusablePool.release(reusable1);
    ReusablePool.release(reusable1);
}

console.log('\n\nOBJECT POOL:');
clientCode();
