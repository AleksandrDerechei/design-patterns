# Object pool

**Object pooling** can offer a significant performance boost; it is most effective in situations where the cost of initializing a class instance is high, the rate of instantiation of a class is high, and the number of instantiations in use at any one time is low.

## How to Implement

1. Create `ObjectPool` class with private array of `Objects` inside.
2. Create `acquire` and `release` methods in ObjectPool class.
3. Make sure that your `ObjectPool` is Singleton

## Rules of thumb

1. The Factory Method pattern can be used to encapsulate the creation logic for objects. However, it does not manage them after their creation, the object pool pattern keeps track of the objects it creates.
2. Object Pools are usually implemented as Singletons.

## Resources

1. [sourcemaking.com object pool](https://sourcemaking.com/design_patterns/object_pool)