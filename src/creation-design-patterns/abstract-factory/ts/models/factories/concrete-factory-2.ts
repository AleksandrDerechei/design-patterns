import { AbstractFactory } from './abstract-factory';
import { AbstractProductA } from '../products/product-a/abstract-product-a';
import { AbstractProductB } from '../products/product-b/abstract-product-b';
import { ConcreteProductA2 } from '../products/product-a/concrete-product-a2';
import { ConcreteProductB2 } from '../products/product-b/concrete-product-b2';

/**
 * Each Concrete Factory has a corresponding product variant.
 */
export class ConcreteFactory2 implements AbstractFactory {
    public createProductA(): AbstractProductA {
        return new ConcreteProductA2();
    }

    public createProductB(): AbstractProductB {
        return new ConcreteProductB2();
    }
}
