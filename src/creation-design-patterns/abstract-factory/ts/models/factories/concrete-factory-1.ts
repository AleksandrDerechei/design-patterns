import { AbstractFactory } from './abstract-factory';
import { AbstractProductA } from '../products/product-a/abstract-product-a';
import { ConcreteProductA1 } from '../products/product-a/concrete-product-a1';
import { ConcreteProductB1 } from '../products/product-b/concrete-product-b1';
import { AbstractProductB } from '../products/product-b/abstract-product-b';

/**
 * Concrete Factories produce a family of products that belong to a single
 * variant. The factory guarantees that resulting products are compatible. Note
 * that signatures of the Concrete Factory's methods return an abstract product,
 * while inside the method a concrete product is instantiated.
 */
export class ConcreteFactory1 implements AbstractFactory {
    public createProductA(): AbstractProductA {
        return new ConcreteProductA1();
    }

    public createProductB(): AbstractProductB {
        return new ConcreteProductB1();
    }
}