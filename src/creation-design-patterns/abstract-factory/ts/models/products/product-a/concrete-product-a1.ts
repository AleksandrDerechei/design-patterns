import { AbstractProductA } from "./abstract-product-a";

/**
import { AbstractProductA } from './abstract-product-a';
 * Concrete Products are created by corresponding Concrete Factories.
 */
export class ConcreteProductA1 implements AbstractProductA {
    public usefulFunctionA(): string {
        return 'Product A1.';
    }
}
