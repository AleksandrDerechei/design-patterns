import { AbstractProductA } from "./abstract-product-a";

export class ConcreteProductA2 implements AbstractProductA {
    public usefulFunctionA(): string {
        return 'Product A2.';
    }
}