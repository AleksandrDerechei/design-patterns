import { AbstractProductB } from "./abstract-product-b";
import { AbstractProductA } from "../product-a/abstract-product-a";

/**
import { AbstractProductB } from './abstract-product-b';
 * Concrete Products are created by corresponding Concrete Factories.
 */
export class ConcreteProductB1 implements AbstractProductB {

    public usefulFunctionB(): string {
        return 'Product B1.';
    }

    /**
     * The variant, Product B1, is only able to work correctly with the variant,
     * Product A1. Nevertheless, it accepts any instance of AbstractProductA as
     * an argument.
     */
    public anotherUsefulFunctionB(collaborator: AbstractProductA): string {
        const result = collaborator.usefulFunctionA();
        return `Product B1 call A1.usefulFunctionA() => (${result})`;
    }
}