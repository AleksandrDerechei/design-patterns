import { AbstractProductB } from "./abstract-product-b";
import { AbstractProductA } from "../product-a/abstract-product-a";

export class ConcreteProductB2 implements AbstractProductB {

    public usefulFunctionB(): string {
        return 'Product B2.';
    }

    /**
     * The variant, Product B2, is only able to work correctly with the variant,
     * Product A2. Nevertheless, it accepts any instance of AbstractProductA as
     * an argument.
     */
    public anotherUsefulFunctionB(collaborator: AbstractProductA): string {
        const result = collaborator.usefulFunctionA();
        return `Product B2 call A2.usefulFunctionA() => (${result})`;
    }
}