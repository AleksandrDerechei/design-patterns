import { Prototype, ComponentWithBackReference } from './prototype';

/**
 * The client code.
 */
function clientCode() {
    const p1 = new Prototype();
    p1.primitive = 245;
    p1.component = new Date();
    p1.circularReference = new ComponentWithBackReference(p1);

    const p2 = p1.clone();

    console.log('\n\nPROTOTYPE:');

    if (p1.primitive === p2.primitive) {
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log('|  Primitive field values have been carried over to a clone. Yay!                          |');
        console.log('--------------------------------------------------------------------------------------------');
    } else {
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log('|  Primitive field values have not been copied. Booo!                                      |');
        console.log('--------------------------------------------------------------------------------------------');
    }

    if (p1.component === p2.component) {
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log('|  Simple component has not been cloned. Booo!                                             |');
        console.log('--------------------------------------------------------------------------------------------');
    } else {
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log('|  Simple component has been cloned. Yay!                                                  |');
        console.log('--------------------------------------------------------------------------------------------');
    }

    if (p1.circularReference === p2.circularReference) {
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log('|  Component with back reference has not been cloned. Booo!                                |');
        console.log('--------------------------------------------------------------------------------------------');
    } else {
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log('|  Component with back reference has been cloned. Yay!                                     |');
        console.log('--------------------------------------------------------------------------------------------');
    }

    if (p1.circularReference.prototype === p2.circularReference.prototype) {
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log('|  Component with back reference is linked to original object. Booo!                       |');
        console.log('--------------------------------------------------------------------------------------------');
    } else {
        console.log('\n--------------------------------------------------------------------------------------------');
        console.log('|  Component with back reference is linked to the clone. Yay!                              |');
        console.log('--------------------------------------------------------------------------------------------');
    }
}

clientCode();