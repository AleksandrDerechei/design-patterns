/**
 * It makes sense to use the Builder pattern only when your products are quite
 * complex and require extensive configuration.
 *
 * Unlike in other creational patterns, different concrete builders can produce
 * unrelated products. In other words, results of various builders may not
 * always follow the same interface.
 */
export class Product1 {
    public parts: string[] = [];

    // Is optional Fetch Method
    public listParts(): void {
        const len = 92; // helpers for demonstration
        const lineWithBounders = `|${new Array(len - 1).join(' ')}|` // helpers for demonstration
        const emptyLineWithBounders = `|${new Array(len - 1).join(' ')}|` // helpers for demonstration
        const startPart = `|  Product parts: ${this.parts.join(', ')}`; // helpers for demonstration

        console.log(emptyLineWithBounders);
        console.log(`${startPart}${lineWithBounders.slice(startPart.length)}`);
    }
}