import { Director } from "./models/builders/director";
import { ConcreteBuilder1 } from "./models/builders/concrete-builder-1";

/**
 * The client code creates a builder object, passes it to the director and then
 * initiates the construction process. The end result is retrieved from the
 * builder object.
 */
function clientCode(director: Director) {
    const builder = new ConcreteBuilder1();
    director.setBuilder(builder);

    console.log('\n\nBUILDER:');

    console.log('\n--------------------------------------------------------------------------------------------');
    console.log('|  Standard basic product:                                                                 |');
    console.log('--------------------------------------------------------------------------------------------');
    director.buildMinimalViableProduct();
    builder.getProduct().listParts();
    console.log('--------------------------------------------------------------------------------------------');

    console.log('\n--------------------------------------------------------------------------------------------');
    console.log('|  Standard full featured product:                                                         |');
    console.log('--------------------------------------------------------------------------------------------');
    director.buildFullFeaturedProduct();
    builder.getProduct().listParts();
    console.log('--------------------------------------------------------------------------------------------');

    // Remember, the Builder pattern can be used without a Director class.
    console.log('\n--------------------------------------------------------------------------------------------');
    console.log('|  Custom product:                                                                         |');
    console.log('--------------------------------------------------------------------------------------------');
    builder.producePartA();
    builder.producePartC();
    builder.getProduct().listParts();
    console.log('--------------------------------------------------------------------------------------------');
}

const director = new Director();
clientCode(director);
console.log('\n\nBUILDER:');